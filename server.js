var express = require('express'),
    cp = require('child_process'),
    os = require('os'),
    fs = require('fs'),
    app = express(),
    port = process.env.PORT || 5001,
    useUncompressedSrc = process.env.UNCOMPRESSED_SRC === 'true',
    useUncompressedLib = process.env.UNCOMPRESSED_LIB === 'true';

console.log('Starting server with variables: %s', JSON.stringify({
        PORT: port,
        UNCOMPRESSED_SRC: useUncompressedSrc,
        UNCOMPRESSED_LIB: useUncompressedLib
    }, null, 4));

// log all requests.
app.all('*', function (req, res, next) {
    var isStatic = req.url.indexOf('/cdn') > -1,
        updateURL = (useUncompressedLib && isStatic) ||
        (useUncompressedSrc && !isStatic);

    console.log('%s %s', req.method, req.url);

    // always use non-minified files in development.
    if (updateURL) {
        if (req.url == '/') {
            req.url = '/index.html';
        }

        if (req.url.indexOf('-min') > 0) {
            req.url = req.url.replace('-min', '');
        }
        else if (req.url.indexOf('.html') > 0 && req.url.indexOf('-src') == -1) {
            req.url = req.url.replace('.html', '-src.html');
        }
    }

    next();
});

// defines the build resources
app.use('/', express.static(__dirname + '/build/html'));
app.use('/', express.static(__dirname + '/build/'));


fs.readFile(__dirname + '/bower.json', {
	encoding: 'utf8'
}, function (err, data) {
	var json;
	if (err) {
		throw err;
	}

	console.log('Serving bower dependencies');
	json = JSON.parse(data);
	Object.keys(json.dependencies).forEach(function (resource) {
		var uri = '/cdn/' + resource.replace('-', '/'),
			path = __dirname + '/bower_components/' + resource;
		console.log('%s - %s', uri, path);
		app.use(uri, express.static(path));	
	});
});
app.use('/cdn', express.static(__dirname + '/static'));

// starting the server.
console.log('\nServer listening on port: %s\n\n', port);

app.listen(port);

// run Grunt watch.
(function() {
    var cmd = (os.platform() === 'win32') ? 'grunt.cmd' : 'grunt',
        grunt = cp.spawn(cmd, [
            '--force',
            'dev'
        ]);

    grunt.stdout.on('data', function(data) {
        // relay output to console
        console.log("%s", data);
    });
})();
function(text, obj) {
	if (typeof text !== 'string') {
		obj = text;
		text = '';
	}

	// Removed for faster rendering.
	// console.log('console-log: ' + text);
	// console.log(obj);

	return [
		text, 
		(JSON) ? JSON.stringify(obj) : obj,
	].join('\n');
}
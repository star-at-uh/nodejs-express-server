var fs = require('fs');

module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            js: {
                files: [
                    'src/js/**/*'
                ],
                tasks: [
                    'jshint:js',
                    'copy:js',
                    'uglify:js'
                ]
            },
            css: {
                files: [
                    'src/css/*.css',
                    'src/css/**/*.css'
                ],
                tasks: [
                    'concat:css',
                    'copy:css'
                ]
            },
            cssmin: {
                files: [
                    'build/css/grunt-generated.css'
                ],
                tasks: [
                    'cssmin'
                ]
            },
            html: {
                files: 'src/html/**/*',
                tasks: [
                    'copy:html'
                ]
            },
            htmlmin: {
                files: 'build/html/**/*',
                tasks: [
                    'htmlmin'
                ]
            },
            hbs: {
                files: 'src/hbs/**/*',
                tasks: [
                    'handlebars',
                    'copy:hbs'
                ]
            },
            hbsmin: {
                files: [
                    'build/hbs/handlebars-template.js'
                ],
                tasks: [    
                    'uglify:hbs'
                ]
            }
        },
        jshint: {
            project: {
                src: [
                    'Gruntfile.js',
                    'server.js'
                ]
            },
            js: {
                files: [{
                    expand: true,
                    cwd: 'src/js',
                    src: '**/*.js',
                    dest: 'build/js'
                }]
            }
        },
        copy: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            js: {
                files: [{
                    flatten: false,
                    expand: true,
                    cwd: 'src/js',
                    src: '**/*.js',
                    dest: 'build/js'
                }]
            },
            css: {
                files: [{
                    flatten: false,
                    expand: true,
                    cwd: 'src/css',
                    src: '**/*.css',
                    dest: 'build/css'
                }]
            },
            html: {
                files: [{
                    flatten: false,
                    expand: true,
                    cwd: 'src/html',
                    src: '**/*.html',
                    dest: 'build/html',
                    ext: '-src.html'
                }]
            },
            hbs: {
                files: [{
                    flatten: false,
                    expand: true,
                    cwd: 'src/hbs',
                    src: '**/*',
                    dest: 'build/hbs'
                }]                
            }
        },
        concat: {
            options: {
                process: function(src, filepath) {
                    return '/** file: ' + filepath + ' */\n' + src;
                }
            },
            css: {
                src: [
                    // default structure
                    'src/css/index.css',
                    'src/css/**/*.css'
                ],
                dest: 'build/css/grunt-generated.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            js: {
                files: [{
                    expand: true,
                    cwd: 'src/js',
                    src: '**/*.js',
                    dest: 'build/js',
                    ext: '-min.js'
                }]
            },
            hbs: {
                files: {
                    'build/hbs/handlebars-template-min.js': [
                        'build/hbs/handlebars-template.js'
                    ]
                }
            }
        },
        cssmin: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            minify: {
                files: {
                    'build/css/grunt-generated-min.css': 'build/css/grunt-generated.css'
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    flatten: false,
                    expand: true,
                    cwd: 'src/html',
                    src: '**/*.html',
                    dest: 'build/html'
                }]
            }
        },
        handlebars: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            compile: {
                options: {
                    opts: {
                        namespace: 'star.template',
                        'module-name': 'star-templates',
                        version: '1.0.0'
                    },
                    helperFile: 'lib/hbs/yui-helper.js',
                    partialFile: 'lib/hbs/yui-partial.js',
                    templateFile: 'lib/hbs/yui-template.js',
                    wrapperFile: 'lib/hbs/yui-wrapper.js',
                    templatePattern: /^.*\/(.+)\.html/i,
                    allTemplatesArePartials: true
                },
                files: {
                    'build/hbs/handlebars-template.js': [
                        'src/hbs/**/*.html'
                    ]
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-flex-handlebars');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');

    // Default task(s).
    grunt.registerTask('default', [
        'jshint:project',
        // Handle javascript tasks
        'jshint:js',
        'copy:js',
        'uglify:js',
        // Handle css tasks
        'concat:css',
        'cssmin',
        // Handle html tasks\
        'copy:html',
        'htmlmin',
        // Handle handlebars tasks
        'handlebars',
        'copy:hbs',
        'uglify:hbs'
    ]);

    grunt.registerTask('dev', [
        'default',
        'watch'
    ]);
};
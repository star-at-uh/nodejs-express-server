# Nodejs express development server

This provides the boilerplate necessary to develop on [Express](http://expressjs.com/) using [Grunt](http://gruntjs.com/) to compile the resources. [Bower](http://bower.io/) manages the web dependencies.

How to Use:
-----------
Add each resource into the respective folder under the `src` folder. Any resource inside the `src/**` folder will be compiled, then save inside the build folder.

### Run the Server

Once all the dependencies have been installed, execute the server script.

	node server.js

Server Configurations:
-----------

Set environment variables on the command line:

##### Windows Example

	set PORT=5002

##### Linux / OSX Example

	export PORT=5002

### Running Express server on a Custom port: `PORT` integer

Set the `PORT` as an varible, then run the server.

### Serving Uncompressed Resources: `UNCOMPRESSED_SRC` boolean

By default, the minified source files will be served. This is so that the live file are prioritized over the development files. Inorder to serve uncompressed source files, add the `UNCOMPRESSED_SRC` variable.

### Serving Uncompressed Resources: `UNCOMPRESSED_LIB` boolean

By default, the minified library files will be served. This is so that the live file are prioritized over the development files. Inorder to serve uncompressed library files, add the `UNCOMPRESSED_SRC` variable.

Dependancy: [NPM](https://www.npmjs.org/)
------------

### Install Project Dependencies

Install `grunt-cli` and `bower` as a global node dependancy.

	npm install -g grunt-cli
	npm install -g bower

### Install the Project

Once `grunt-cli` and `bower` have been installed, install the Express server dependencies.

	npm install


The Grunt task will be spawned in a child process while the server is running.

Dependancy: [Express](http://expressjs.com/)
-----------

### Routing URI
The server will serve the static and built resources in the descending priority:

URL				| Path			| Example
-------------	| -------------	| -----------------
/				| ~/build/html	| http://localhost:5001/index.html
/				| ~/build/		| http://localhost:5001/css/grunt-generated.css
/cdn		  	| ~/static		| http://localhost:5001/cdn/purecss/0.3.0/pure-min.css

Dependancy: [Grunt](http://gruntjs.com)
-----------

### Available Grunt tasks

Run the default build

	grunt

Runs the default build, then watches the source directories.

	grunt dev

Installs any bower resource files to the `static` folder.

	grunt bowercopy

### How the Grunt default build works

1. Installs any external web dependancies.
2. Lints the project build files.
3. Lints the JavaScript source.
4. Copies the JavaScript files from `src/js` to `build/js`.
5. Uglifies the JavaScript files from `src/js` to `build/js`. Adds the `-min.js` extension.
6. Concatenates all the CSS files together in `src/css` to `build/css/grunt-generated.css`. Order can be specified in `Gruntfile.js`.
7. Minifies the CSS in `build/css/grunt-generated.css` to `build/css/grunt-generated-min.css`.
8. Copies the HTML files in `src/html` into `build/html`. Adds the `-src.html` extension.
9. Minifies the HTML files in `src/html` into `build/html`.
10. Combines the Handlebar templates in `src/hbs`into `build/handlebars-template.js`.
11. Uglifies the compiled Handlebar template `build/handlebars-template.js` to `build/handlebars-template-min.js`.

Dependancy: [Bower](http://bower.io/)
----------

### Downloading Modules

#### Manual Download

Installs the web dependencies in the `bower.json` file.

	bower install

### Other commands

More information can be found in the [bower documentation](http://bower.io/).
